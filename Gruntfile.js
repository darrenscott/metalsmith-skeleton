// Grunt build file for the Keech Green web site project

module.exports = metalsmithLogPlugin;

function metalsmithLogPlugin() {
    return function(files, metalsmith, done) {
        for (var file in files) {
            var f = files[file];
            console.log(f);
        }
        done();
    }
};

module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            build: {
                src: ['build']
            }
        },
        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['*.html'],
                    dest: 'build/'
                }, {
                    expand: true,
                    cwd: 'bower_components',
                    src: ['**/*'],
                    dest: 'build/bower_components'
                }]
            }
        },
        uglify: {
            options: {
                report: 'min',
                mangle: false
            },
            build: {
                src: 'src/js/**/*.js',
                dest: 'build/js/<%= pkg.name %>.min.js'
            }
        },
        connect: {
            server: {
                options: {
                    open: 'http://127.0.0.1:9001',
                    hostname: '*',
                    port: 9001,
                    base: 'build',
                    livereload: 35721,
                    debug: true
                }
            }
        },
        watch: {
            html: {
                files: ['src/**/*.md', 'src/*.html', 'src/css/*.css'],
                tasks: ['metalsmith']
            },
            templates: {
                files: ['templates/**/*.hbs'],
                tasks: ['metalsmith']
            },
            images: {
                files: ['src/**/*.jpg', 'src/**/*.png'],
                tasks: ['metalsmith']
            },
            //js: {
            //    files: ['src/js/*.js'],
            //    tasks: ['uglify']
            //},
            livereload: {
                options: {
                    livereload: '<%= connect.server.options.livereload %>',
                    event: ['changed']
                },
                files: [
                    'build/*.html', 'build/css/*.css', 'build/js/*.js', 'build/images/**/*.jpg'
                ]
            }
        },
        sass: {
            dist: {
                files: {
                    'dest/css/style.css': 'src/sass/style.scss'
                }
            }
        },
        metalsmith: {
            skeleton: {
                options: {
                    metadata: {
                        "version": "1.0.0",
                        "meta-description": "Skeleton Grunt / Metalsmith Static Site",
                        "meta-keywords": "Grunt, Metalsmith, Static, Skeleton"
                    },
                    plugins: {
                        'metalsmith-build-date': {},
                        'metalsmith-markdown': {},
                        'metalsmith-path': {},
                        'metalsmith-templates': {
                            engine: 'handlebars',
                            partials: {
                                'masthead': 'partials/masthead',
                                'footer': 'partials/footer'
                            }
                        },
                        //'metalsmithLogPlugin': {},
                        //'metalsmith-uglify': {
                        //    filter: [
                        //    ],
                        //    concat: 'script.min.js',
                        //    preserveComments: 'none'
                        //},
                        'metalsmith-ignore': [
                        ]
                    }
                },
                src: 'src',
                dest: 'build'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-metalsmith');

    grunt.registerTask('build', ['clean', 'copy', 'sass', 'uglify', 'metalsmith']);
    grunt.registerTask('default', ['build', 'connect:server', 'watch']);

};