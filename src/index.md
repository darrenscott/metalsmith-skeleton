---
template: index.hbs
items:
  - This is item 1
  - And this is item 2
  - Get ready for item 3
  - And to finish, item 4
---
This is the Markdown format body content.
